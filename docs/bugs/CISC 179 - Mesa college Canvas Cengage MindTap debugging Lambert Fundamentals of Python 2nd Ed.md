#### Link that was broken for students but I fixed:
https://gateway.cengage.com/rest/launchBasicLTI/231451/6033602314518262512703421494/3716616

https://ng.cengage.com/static/nb/ui/evo/index.html?snapshotId=3371555&id=1735553225&deploymentId=6033602314518262512703421494&eISBN=9780357505397
https://sdccd.instructure.com/courses/2450635/modules/items/44270250

#### Registered new student cengage account 
- student-cisc-179-21616@totalgood.com
- password in bitwarden
- 

#### 

Two different IDs for MindTap dashboards:
"Lambert Fundamentals of Python: First Programs 2e v2"
#### 1. created 2/3 with Natasha? 
https://ng.cengage.com/static/nb/ui/evo/index.html?snapshotId=3371555&id=1735553225&deploymentId=6033602314518262512703421494&eISBN=9780357505397

#### 2. created 2/11 when students complained? 
"Fundamentals of Python: First Programs, 2e"
https://ng.cengage.com/static/nb/ui/evo/index.html?snapshotId=3379150&id=1739977816&eISBN=9781337560115


# Pre-Course Assessment Quiz (Practice)

### 1
What effect does the following print statement have? print("Hello" * 5)
  a. The string "Hello" is converted into an int type of value 1 and then multiplied by 5, producing "5".   
  b. The string is indexed and the character count is multiplied with the total number of characters in the string, producing "25".   
  c. The print statement produces a type error as you cannot multiply a string type with an integer type.   
  d. * The print statement will produce "HelloHelloHelloHelloHello".

### 2
What is a one-way selection statement in the context of Python?
  a. * It is a single if statement without a corresponding else statement used to test a condition and run statements, then proceed to the next statement.  
  b. It is a single if-else statement block that is used to test a condition and run statements, as well as provide a default path for execution.   
  c. It is a switch statement block that is used to direct the flow of data based on a single condition.  
  d. It is an if statement that tests no conditions but provides a block for execution. 

### 3
What two methods can be used to return a copy of a string named s converted to all lowercase and all uppercase respectively?
a. * s.lower()  
b. s.lowcase()  
c. * s.upper()  
d. s.upcase()   

### 4
What statement accurately describes the use of a parameter in Python?
  a. * A parameter is the name used in the function definition for an argument that is passed to the function when it is called.  
  b. A parameter is the name used for a function that is then referenced by other parts of the program.   
  c. A parameter is the name used for data that is returned from the execution of a function.   
  d. A parameter is the name given to an acceptable range of values that are output from a program.   

### 5
The gradual process of developing functions to solve each subproblem in a top-down design is known as what process?
  a. procedural refinement  
  b. * stepwise refinement  
  c. progressing resolution   
  d. incremental solving  

### 6
What is the maximum number of distinct color values that can be displayed by the true color RGB system?
  a. 256  
  b. 16384  
  c. 32,768   
  d. * 16,777,216   

### 7 
How can you associate a keyboard event and an event-handling method with a widget?
  a. You must name the event-handling method "onPress_<key>".   
  b. * You must use the bind method, which expects a string containing a key event.   
  c. You must map a shortcut in the host operating system to run the script with a specific argument.   
  d. You must define the key's event in a YAML-based configuration file.  

### 8
What is the process in which the Python virtual machine recycles its storage known as?
    a. * garbage collection 
    b. trash expunging  
    c. data dumping     
    d. tombstoning  

### 9
After the creation of a thread, what makes a thread "ready"?
    a. * The calling of the start method.     
    b. The calling of the ready method.     
    c. The instantiation of the thread, followed by the use of the "begin" function.    
    d. The creation of the thread itself places it in a "ready" state.  

### 10

What is the dominant term when evaluating the amount of work in an algorithm?
    a. When expressed as time vs. work performed, the dominant term is the area where the least amount of time is expended.     
    b. When expressed as a matrix of related problems, the problem that is most significant becomes the dominant term.  
    c. When expressed as a quadratic function, the dominant term is the statement in the algorithm where the fastest work is performed.     
    d. * When expressed as a polynomial, the dominant term of an algorithm is the area where the most work is performed.  
