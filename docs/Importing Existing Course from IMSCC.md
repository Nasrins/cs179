# Importing Existing Course

1. Create a new Empty Course
2. Choose Import/Export in righthand drawer/popout menu
3. Browse to an `.imscc` file (may need to unzip a zip file?)
